# used to populate vocab list. With large text takes too long to complete and also google translate API eventually
# blocks ip, so interim step is to pickle dictionary and use pop_pbin_to_web to add what has been processed to vocab
# database. The code automatically remembers where it last got stuck and starts from there.
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from rango.models import Category, Vocab, HSK, Source
from requests.exceptions import ConnectionError
from collections import Counter
from jieba_fast import cut as j
from jieba_fast import posseg as pseg

from googletrans import Translator
from pinyin_jyutping_sentence import pinyin as pinyin
import re
import time
from populate.CEdict_parser import main

from pprint import pprint


import collections

from populate.pickle_dat import pickling, unpickling
import requests

import sys
filename = sys.argv[1]

def populate(filename):

    flag_ext2 = {"n": "Common nouns",
                "f": "Orientation noun",
                "s": "Premises noun",
                "t": "Time",
                "nr": "Name",
                "ns": "Place names",
                "nt": "Organization name",
                "nw": "Title",
                "nz": "Other proper names",
                "v": "Common verbs",
                "vd": "Verb",
                "vn": "Noun verb",
                "a": "Adjective",
                "ad": "Adverb",
                "an": "Nouns",
                "d": "Adverb",
                "m": "Quantifier",
                "q": "quant.",
                "r": "Pronoun",
                "p": "Preposition",
                "c": "Conjunction",
                "u": "Particle",
                "xc": "Other word",
                "w": "Punctuation",
                "PER": "Name",
                "LOC": "Place Name",
                "ORG": "Organization Name",
                "TIME": "Time"}

    flag_ext = {'j':'Abbreviations',
                'g':'Academic vocabulary',
                'a':'Adjective',
                'al':'Adjective idiom',
                # 'ag':'Adjective morpheme',
                'ad':'Adverb',
                'd':'Adverb',
                'vd':'Adverb',
                'ag':'Adverbial',
                'dg':'Adverbial',
                # 'dg':'Adverbs',
                'nic':'Affiliates',
                'nis':'Agency suffix',
                'Rg':'Ancient Chinese pronoun morpheme',
                'nba':'Animal name',
                'uyy':'As common as',
                'ntcb':'Bank',
                'nb':'Biological name',
                'gb':'Biology related words',
                'gbc':'Biotype',
                'nbp':'Botanical name',
                'ude1':'Bottom of',
                'nmc':'Chemical name',
                'gc':'Chemistry related vocabulary',
                'wm':'Colon, full-width :: half-width::',
                'wd':'Comma, full-width :, half-width :,',
                'ntc':'Company Name',
                'nr1':'Compound surname',
                'gi':'Computer related vocabulary',
                'c':'Conjunction',
                'ul':'Conjunction',
                'uv':'Conjunction',
                'cc':'Coordinate conjunction',
                'wp':'Dash, full angle: —— —— —— － half angle: —— —',
                'rz':'Demonstrative',
                'nhd':'Disease',
                'bg':'Distinctive morpheme',
                'bl':'Distinguish part of speech idioms',
                'b':'Distinguishing words',
                'nhm':'Drug',
                'nit':'Education related institutions',
                'nts':'Elementary and secondary schools',
                'ws':'Ellipses, full-width: ...',
                'wt':'Exclamation mark, full-width :!',
                'ntcf':'Factory',
                'nf':'Food, such as "potato chips"',
                'vx':'Formal verb',
                'wj':'Full stop, full angle :.',
                'gg':'Geology and Geology',
                'ude3':'Get',
                'nto':'Government agency',
                'ude2':'Ground',
                'nh':'Health related terms such as medical diseases',
                'nth':'Hospital',
                'ntch':'Hotel Guest House',
                'i':'Idiom',
                'l':'Idiom',
                'udh':'if',
                'uls':'In terms of speaking',
                # 'nt':'Institution name',
                'nt':'Institutional groups',
                'ni':'Institution-related (not the name of an independent institution)',
                'e':'Interjection',
                'rys':'Interrogative pronoun',
                'ry':'Interrogative pronouns',
                'vi':'Intransitive verb (internal verb)',
                'nm':'Item name',
                'nrj':'Japanese name',
                'nn':'Job-related nouns',
                'wkz':'Left bracket, full angle: (〔[{{"" [〖<half angle: ([{<',
                # 'wyz':'Left quote, full angle: "'『',
                'nx':'Letter proper name',
                'ulian':'Lian ("Even Primary School Students")',
                'dl':'Link',
                's':'Location word',
                'gm':'Math related vocabulary',
                # 'y':'Modal',
                'y':'Modal particle (delete yg)',
                'nr2':'Mongolian name',
                # 'g':'Morpheme',
                # 'vg':'Morpheme',
                'nr':'Name',
                'an':'Nominal form',
                'nl':'Nominal idiom',
                'ng':'Nominal morpheme',
                'vn':'Nominal verb',
                # 'x':'Non-morpheme',
                'xx':'Non-morpheme',
                'n':'Noun',
                'mg':'Number morpheme',
                'm':'Numeral',
                'Mg':'Numerals like A, B, C',
                'nnd':'Occupation',
                'uzhi':'Of',
                'ule':'Okay',
                'end':'Only for final ## Final',
                'o':'Onomatopoeia',
                'nz':'Other proper names',
                'u':'Particle',
                'ud':'Particle',
                'uj':'Particle',
                'ug':'Past',
                'uguo':'Past',
                'wb':'Percent sign, thousand sign, full angle:% ‰ half angle:%',
                'rr':'Personal Pronouns',
                'gp':'Physics related vocabulary',
                'usuo':'Place',
                'ns':'Place name',
                'nnt':'Position / Title',
                'f':'Position of the word',
                # 'h':'Predecessor',
                'rzv':'Predicate demonstrative pronoun',
                'ryv':'Predicate interrogative pronoun',
                'h':'Prefix',
                'rzs':'Premise pronoun',
                'p':'Preposition',
                'r':'Pronoun',
                'rg':'Pronoun morpheme',
                'w':'Punctuation',
                'q':'quantifier',
                'mq':'Quantifier1',
                'qg':'Quantifier2',
                'qt':'Quantifier3',
                'qv':'Quantifier4',
                'ww':'Question mark, full-width :?',
                'wky':'Right parenthesis, full-width :)〕]｝》】〉 Half-width:)] {>',
                # 'wyy':'Right quote, full-width: "'』',
                'wf':'Semicolon, full-width :; half-width :;',
                'z':'Status word',
                'zg':'Status word',
                'x':'String',
                # 'k':'Subsequent ingredients',
                'k':'Suffix',
                'tg':'Temporal part-of-speech',
                # 'tg':'Tenor',
                'pba':'The preposition "Ba"',
                'pbei':'The preposition "be"',
                'ntu':'the University',
                'vyou':'The verb "有"',
                'ryt':'Time interrogative pronoun',
                'rzt':'Time pronoun',
                't':'Time word',
                'wn':'Ton, full-width :,',
                'yg':'Tone morpheme',
                'nsf':'Transliterated place names',
                'nrf':'Transliteration',
                'vf':'Trending verb',
                'wh':'Unit symbol, full-width: ¥ ＄ ￡ ° ℃ half-width: $',
                'un':'Unknown word',
                'xu':'URL URL',
                'v':'Verb',
                'vshi':'Verb "Yes"',
                'vl':'Verb idiom',
                'vg':'Verb morpheme',
                'udeng':'Wait wait wait',
                'uz':'Write',
                'uzhe':'Write',
                'nbc':'Zoology'
                }

    CEdict = main()
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')
    POP_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/populate/')
    print(TEXT_DIR)
    # filename = "Harry_Potter_Mandarin"
    # filename = input("Enter filename (OR press enter for Journey to the West西游记_Wu Cheng'en吴承恩): ") or "Journey to the West西游记_Wu Cheng'en吴承恩"


    # inputPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/static/texts/Harry_Potter_Mandarin.txt"
    # flagPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/populate/"
    with open(TEXT_DIR + filename + '.txt', encoding="utf8", errors='ignore') as f:
        inputSample = f.read()

    inputSample_list = j(inputSample, cut_all=False)
    inputSampleCnt = dict(Counter(inputSample_list))

    # posDict = {}

    print('Identifying categories...')
    posDict = unpickling(POP_DIR + filename + 'flags')

    if not posDict['data']:
        words = pseg.cut(inputSample)
        for w in words:
            posDict[w.word] = w.flag

        pickling(posDict, POP_DIR + filename + 'flags')

    try:
        picklecount = unpickling(POP_DIR + filename + 'count')['data'] #.pop()
        print(picklecount)
    except TypeError:
        picklecount = 0

    if not picklecount:
        picklecount = 0

    vocab = unpickling(POP_DIR + filename + 'vocab')['data']
    if not vocab:
        vocab = []

    sorted_inputSampleCnt = sorted(inputSampleCnt.items(), key=lambda kv: kv[1], reverse=True)
    print(sorted_inputSampleCnt)

    print(len(sorted_inputSampleCnt))

    print("Creating vocabulary dictionary...")


    count = 0

    for x, freq in sorted_inputSampleCnt:

        if re.search("[\u4e00-\u9FFF]", x):

            count += 1



            if int(picklecount) < count:
                # print("if statement entered: ")
                try:
                    CEdict_translated = next(item for item in CEdict if item["simplified"] == x)

                    k = CEdict_translated['english']
                    print('slow-tracked: ' + str(picklecount) + '/' + str(count) + '/' + str(len(sorted_inputSampleCnt)) + '\tCEd:\t\t' + x + '\t' + k)
                except StopIteration:
                    try:
                        translator = Translator()
                        g = translator.translate
                        k = g(x).text
                        print('slow-tracked: ' + str(picklecount) + '/' + str(count) + '/' + str(len(sorted_inputSampleCnt)) + '\tgoogled:\t\t' + x + '\t' + k)
                    except AttributeError:
                        print('AttributeError:', x)
                        k = x
                        pass
                    except ValueError:
                        k = x
                        pass
                    except ConnectionError as e:  # This is the correct syntax
                        k = x
                        pass
                    except requests.exceptions.Timeout:
                        k = x
                        pass

                pY = pinyin(x, spaces=False)

                try:
                    curr_cat = posDict[x]
                except KeyError:
                    curr_cat = 'other'

                att = {'category': curr_cat, 'chinese': x, 'score': 0, 'frequency': int(freq), 'english': k,
                       'previous': 0, 'pinyin': pY, 'learnt': 0}
                vocab.append(att)

                pickling(vocab, POP_DIR + filename + 'vocab')
                pickling(count, POP_DIR + filename + 'count')



            else:
                try:
                    CEdict_translated = next(item for item in CEdict if item["simplified"] == x)
                    k2 = CEdict_translated['english']
                    print('fast-tracked:' + str(picklecount) + '/' + str(count) + '/' + str(len(sorted_inputSampleCnt)) + '\tCEd:\t\t' + x + '\t' + k2)
                except StopIteration:

                    # translator = Translator()
                    # g = translator.translate
                    # k = g(x).text
                    # print(str(count) + '/' + str(len(sorted_inputSampleCnt)) + '\tGoogled:\t' + x + '\t' + k)
                    pass

                # try:
                #
                #     translator = Translator()
                #     g = translator.translate
                #     k = g(x).text
                #     print(str(count) + '/' + str(len(sorted_inputSampleCnt)) + '\tGoogled:\t' + x + '\t' + k)
                #
                # except ValueError:
                #     try:
                #         print('Google translate prob. blocked IP')
                #         CEdict_translated = next(item for item in CEdict if item["simplified"] == x)
                #         k = CEdict_translated['english']
                #     except StopIteration:
                #         k = g(x).text
                #         pass
                # except ConnectionError as e:  # This is the correct syntax
                #
                #     print(e)
                #     try:
                #         print('Google translate probably blocked IP')
                #         CEdict_translated = next(item for item in CEdict if item["simplified"] == x)
                #         k = CEdict_translated['english']
                #     except StopIteration:
                #         k = g(x).text
                #         pass
                #
                # except requests.exceptions.Timeout:
                #     try:
                #         print('Google translate prob. blocked IP')
                #         CEdict_translated = next(item for item in CEdict if item["simplified"] == x)
                #         k = CEdict_translated['english']
                #     except StopIteration:
                #         k = g(x).text
                #         pass


                        # time.sleep(5)
                # print(att)
        # except ValueError:
        #     print('Google translate prob. blocked IP')
        # except ConnectionError as e:  # This is the correct syntax
        #     print(e)
    print(vocab)
    print("Finished. Pickled files ready for putting in database.")
    # print("Grouping vocabulary to categories...")
    #
    # grouped = collections.defaultdict(list)
    # for item in vocab:
    #     grouped[item['category']].append(item)
    # cats = {}
    # for category, group in grouped.items():
    #     print('')
    #     print(category)
    #     pprint(group, width=150)
    #
    #     cats[category] = {'vocabulary': group}
    #
    # print(cats)
    # s = add_source(filename)
    # for cat, cat_data in cats.items():
    #     c = add_cat(s, cat)
    #     for p in cat_data['vocabulary']:
    #         v = add_vocab(c, p['chinese'], p['english'], p['pinyin'], p['learnt'], p['score'], p['previous'], p['frequency'])
    #         v.sources.add(s)
    #         v.save()
    # # Print out the categories we have added.
    # for c in Category.objects.all():
    #     for p in Vocab.objects.filter(category=c):
    #         print(f'- {c}: {p}')


def add_vocab(source, category, hsk, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0, hsk_lvl=0):
    p = Vocab.objects.get_or_create(source=source, category=category, hsk=hsk, chinese=chinese, english=english, pinyin=pinyin)[0]
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.hsk_lvl=hsk_lvl
    p.save()
    return p


def add_cat(name):

    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c

def add_hsk(name):
    h = HSK.objects.get_or_create(hsk_level=name)[0]
    h.save()
    return h


def add_source(name):
    s = Source.objects.get_or_create(title=name)[0]
    s.save()
    return s

# Start execution here!
if __name__=='__main__':

    print('Starting Kanshu population script...')
    populate(filename)