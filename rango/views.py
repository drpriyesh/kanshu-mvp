from django.template import RequestContext
from django.shortcuts import render, redirect
from rango.forms import CategoryForm, VocabForm, SentenceForm, ContactForm, EditForm, UserEditForm, UserVocabForm
from rango.forms import UserForm, UserProfileForm, UserUploadForm
from rango.edit import editword, usereditword
from django.template.loader import render_to_string
from django.http import HttpResponse
from rango.models import Chapter, About, HSK, Category, Vocab, Sentence, Source, MotherText, UserSource, UserPrevious, UserVocab, UserCategory, UserSentence, UserBookmark, UserDictionary
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from datetime import datetime
from googletrans import Translator
from rango.kanshu import run_kanshu
from rango.user_kanshu import doc_process, binfile_to_db, run_user_kanshu
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from rango.models import UserProfile
from django.views import View
from django.utils.decorators import method_decorator

from django.contrib import messages
from django.contrib.auth.forms import PasswordResetForm
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.core.mail import send_mail, BadHeaderError
from django.db.models.query_utils import Q


import json
from django.http import JsonResponse
import string
from django.core.files.storage import FileSystemStorage

def text2speechtest(request):

    context_dict = {}

    return render(request, 'rango/text2speechtest.html', context=context_dict)

def password_reset_request(request):
    if request.method == "POST":
        password_reset_form = PasswordResetForm(request.POST)
        if password_reset_form.is_valid():
            data = password_reset_form.cleaned_data['email']
            associated_users = User.objects.filter(Q(email=data))
            if associated_users.exists():
                for user in associated_users:
                    subject = "Password Reset Requested"
                    email_template_name = "password/password_reset_email.txt"
                    c = {
                    "email":user.email,
                    'domain':'http://bookshelf.kanshuchinese.com',
                    'site_name': 'Kanshu',
                    "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                    "user": user,
                    'token': default_token_generator.make_token(user),
                    'protocol': 'http',
                    }
                    email = render_to_string(email_template_name, c)
                    try:
                        send_mail(subject, email, 'kanshuapp@gmail.com', [user.email], fail_silently=False)
                    except BadHeaderError:
                        return HttpResponse('Invalid header found.')
                    return redirect("/password_reset/done/")
    password_reset_form = PasswordResetForm()
    return render(request=request, template_name="password/password_reset.html", context={"password_reset_form":password_reset_form})




def useredit(request):
    user= request.user
    delete_session(request, 'result_list')
    delete_session(request, 'sides')
    form = UserVocabForm()
    if request.method == 'POST':
        form = UserVocabForm(request.POST, current_user=request.user)
        if form.is_valid():
            newword = form.save(commit=False)
            print('user.id: ' + str(user.id))
            print('newword.source.id: ' + str(newword.source.id))
            print('newword.chinese: ' + str(newword.chinese))

            try:

                v = UserVocab.objects.get(user_id=user.id, source_id=newword.source.id, chinese=newword.chinese)
                print(str(v))
                frequency = v.frequency
                v.delete()

                print('deleted v')
            except:
                print('editing word not found')
                frequency = 1
            newword.user = user
            pinyin = usereditword(user, newword.source, newword.chinese, newword.category)

            newword.pinyin = pinyin
            newword.frequency = frequency


            newword.save()
            return redirect(reverse('rango:personal_kanshu_index'))
        else:

            print(form.errors)

    return render(request, 'rango/useredit.html', {'form': form})


def edit(request, source_title_slug):
    print('source_title_slug:', source_title_slug)
    s = source_helper(source_title_slug.lower())
    source = s['source_id']
    print('source: ', str(source))
    context_dict = {}
    context_dict['source'] = str(source).lower()
    delete_session(request, 'result_list')
    delete_session(request, 'sides')
    form = EditForm()

    if request.method == 'POST':

        form = EditForm(request.POST)
        if form.is_valid():
            word = form.cleaned_data['word']
            print('word: '+ word)
            tag = form.cleaned_data['tag']
            print('tag: ' + tag)
            english = form.cleaned_data['english']
            print('english: ' + english)
            source = s['source_id']
            print('source from post is: ' + str(source))
            words = {'word':word, 'tag':tag, 'english': english}
            editword(s, words)
            return redirect(reverse('rango:kanshu_sentence', kwargs={'source_title_slug': source_title_slug}))

        else:
            print(form.errors)
    context_dict['form'] = form
    return render(request, 'rango/edit.html', context=context_dict)


def contact_us(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            # send email code goes here
            sender_name = form.cleaned_data['name']
            sender_email = form.cleaned_data['email']

            message = "{0} of {1} has sent you a new message:\n\n{2}".format(sender_name, sender_email, form.cleaned_data['message'])
            send_mail('New Enquiry', message, 'kanshuapp@gmail.com', ['kanshuchinese@gmail.com'], fail_silently=False)
            messages.success(request, "Message sent!")
            return HttpResponse('<h1>Thanks for contacting me! Click back button</h1>')
    else:
        form = ContactForm()

    return render(request, 'rango/contact_us.html', {'form': form})


def cover(request):
    context_dict = {}


    # Render the response and send it back!
    return render(request, 'rango/cover.html', context=context_dict)

# hgjiyhkhjkhjklh
def addremovebook(request, source_title_slug):

    s = Source.objects.filter(slug=source_title_slug)

    for i in r:

        s = Source.objects.get(title=i['title'])
        word_count=len(list(Vocab.objects.filter(source=s).values('chinese')))

        print(i['title'])
        title.append(i['title'])
        text.append(i['type'])
        author.append(i['author'])
        content.append((i['title'], i['type'], i['author'], i['slug'], i['picture'], str(word_count)))

    context_dict = {}
    s = source_helper(request.user)
    context_dict['title'] = s['title']
    # Render the response and send it back!

    def add_vocab(category, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0):
        p = Vocab.objects.get_or_create(category=category, chinese=chinese, english=english, pinyin=pinyin)[0]
        p.score = score
        p.frequency = frequency
        p.previous = previous
        p.learnt = learnt
        p.save()
        return p

    return render(request, 'rango/cover.html', context=context_dict)



def bookshelf(request):
    context_dict = {}
    r = list(Source.objects.filter(language="Mandarin").values('title', 'type', 'author', 'picture', 'about_book', 'about_author'))

    title=[]
    text=[]
    author=[]
    content=[]
    about_book=[]
    about_author=[]

    for i in r:

        s = Source.objects.get(title=i['title'])
        word_count=len(list(Vocab.objects.filter(source=s).values('chinese')))

        print(i['title'])
        title.append(i['title'])
        text.append(i['type'])
        author.append(i['author'])
        about_author.append(i['about_author'])
        about_book.append(i['about_book'])
        content.append((i['title'], i['type'], i['author'], i['picture'], str(word_count)))

    context_dict['titles'] = title
    context_dict['texts'] = text
    context_dict['authors'] = author
    context_dict['contents'] = content



    print(about_author, about_book)
    # Render the response and send it back!
    return render(request, 'rango/bookshelf.html', context=context_dict)

@login_required
def bookshelf_user(request):
    context_dict = {}
    r = list(Source.objects.filter(language="Mandarin").values('title', 'slug', 'type', 'author', 'picture', 'about_book', 'about_author'))

    print(r)
    title=[]
    text=[]
    author=[]
    content=[]
    about_book=[]
    about_author=[]

    for i in r:

        s = Source.objects.get(title=i['title'])
        word_count=len(list(Vocab.objects.filter(source=s).values('chinese')))

        title.append(i['title'])
        text.append(i['type'])
        author.append(i['author'])
        about_author.append(i['about_author'])
        about_book.append(i['about_book'])
        content.append((i['about_author'], i['about_book'], i['title'], i['type'], i['author'], i['picture'], i['slug'], str(word_count)))

    context_dict['titles'] = title
    context_dict['texts'] = text
    context_dict['authors'] = author
    context_dict['contents'] = content

    for line in context_dict:
        print("\n")
        print(line, '\n')

    print('\n\ncontext_dict: ', context_dict)
    # Render the response and send it back!
    return render(request, 'rango/bookshelf_user.html', context=context_dict)


@login_required
def personal_kanshu_source(request, source_title_slug):
    context_dict = {}
    result_list = []

    user = request.user
    source = UserSource.objects.get(user=user, slug=source_title_slug)
    print('source_title_slug: ' + source_title_slug)
    if request.method == 'GET':
        try:
            sentence_number = request.session['sentence_number']
        except KeyError:
            sentence_number = 1

        r = run_user_kanshu(sentence_number, user, source_title_slug)
        context_dict['result_list'] = r['results']
        request.session['sides'] = r['sides']
        request.session['sentence_number'] = sentence_number

    if request.method == 'POST':
        sentence_number = request.POST['sentence_number'].strip()

        if sentence_number:
            # m = mother_text_helper(sentence_number)
            r = run_user_kanshu(sentence_number, user, source_title_slug)
            context_dict['result_list'] = r['results']
            request.session['sides'] = r['sides']
            request.session['sentence_number'] = sentence_number
            # request.session['mother_text'] = m

    kanshu_cookie_handler(request)
    context_dict['sides'] = request.session['sides']
    context_dict['sentence_number'] = request.session['sentence_number']
    context_dict['adjust'] = request.session['adjust']
    context_dict['source'] = source
    # context_dict['mother_text'] = request.session['mother_text']

    return render(request, 'rango/personal_kanshu.html', context=context_dict)

def user_index_cookie_handler(request):
    request.session['uservocabulary_list'] = get_server_side_cookie(request, 'uservocabulary_list', [])



def personal_kanshu_index_delete(request):
    context_dict = {}
    user = request.user
    source_list = UserSource.objects.filter(user=user)
    source_list = source_list.order_by('-title')
    print(source_list)
    context_dict['sources'] = source_list
    return render(request, 'rango/personal_kanshu_index_delete.html', context=context_dict)


def personal_kanshu_source_delete(request, source_title_slug):
    context_dict = {}
    user = request.user
    s = UserSource.objects.get(user=user, slug=source_title_slug)
    UserVocab.objects.filter(user=user, source=s).delete()
    s.delete()
                          #).sources.remove(s)

    source_list = UserSource.objects.filter(user=user)
    source_list = source_list.order_by('-title')
    context_dict['sources'] = source_list
    try:
        del request.session['uservocabulary_list']
    except KeyError:
        pass

    return render(request, 'rango/personal_kanshu_index_delete.html', context=context_dict)


def delete_session(request, sessName):
    try:
        del request.session[sessName]
    except KeyError:
        pass

@login_required
def personal_kanshu_index(request):

    delete_session(request, 'result_list')
    delete_session(request, 'sides')
    delete_session(request, 'sentence_number')
    delete_session(request, 'adjust')
    delete_session(request, 'source')

    user = request.user
    context_dict = {}
    user_profile = UserProfile.objects.get_or_create(user=user)[0]
    if request.method == 'GET':

        user_profile.document.delete()
        form = UserUploadForm({'website': user_profile.website,
                                'picture': user_profile.picture,
                                'document': user_profile.document})
        context_dict['form']=form

    if request.method == 'POST':
        try:
            del request.session['uservocabulary_list']
        except KeyError:
            pass
        form = UserUploadForm(request.POST, request.FILES, instance=user_profile)
        if form.is_valid():
            form.save(commit=True)
            if user_profile.document:
                x = doc_process(user)
                y = binfile_to_db(user)
                user_profile.document.delete()
        else:
            print(form.errors)
        context_dict = {'user_profile': user_profile,
                        'selected_user': user,
                        'form': form}
        print("Processing document: doc_process")

        return redirect(reverse('rango:personal_kanshu_index'))

    source_list = UserSource.objects.filter(user=user)
    source_list = source_list.order_by('-title')

    context_dict['sources'] = source_list
    usercat_list = list(UserCategory.objects.filter(user=user).values('category', 'name'))

    context_dict['usercat'] = usercat_list
    if 'uservocabulary_list' not in request.session:
        print('slow')
        uservocabulary_list = list(UserVocab.objects.filter(user=user).values('category__name',
                                        'hsk_lvl', 'frequency', 'chinese', 'pinyin', 'english', 'source__title'))
        request.session['uservocabulary_list'] = uservocabulary_list
    else:
        print('fast')
        user_index_cookie_handler(request)
        uservocabulary_list = request.session['uservocabulary_list']

    # create pages
    page = request.GET.get('page', 1)
    paginator = Paginator(uservocabulary_list, 10000)
    try:
        uservocabulary = paginator.page(page)
    except PageNotAnInteger:
        uservocabulary = paginator.page(1)
    except EmptyPage:
        uservocabulary = paginator.page(paginator.num_pages)

    context_dict['boldmessage'] = 'Register/Login and head to the Kanshu Carousel tab to begin reading!'
    context_dict['uservocabulary'] = uservocabulary

    # Render the response and send it back!
    # Call the helper function to handle the cookies
    visitor_cookie_handler(request)


    return render(request, 'rango/personal_kanshu_index.html', context=context_dict)

def encode_url(str):

    return str.replace(' ', '_')


def decode_url(str):

    return str.replace('_', ' ')


def get_category_list(max_results=0, starts_with=''):

    cat_list = []
    if starts_with:
        cat_list = Category.objects.filter(name__startswith=starts_with)
    else:
        cat_list = Category.objects.all()

    if max_results > 0:
        if (len(cat_list) > max_results):
            cat_list = cat_list[:max_results]

    for cat in cat_list:
        cat.url = encode_url(cat.name)

    return cat_list


class CategorySuggestionView(View):
    def get(self, request):
        if 'suggestion' in request.GET:
            suggestion = request.GET['suggestion']
        else:
            suggestion = ''
        category_list = get_category_list(max_results=8, starts_with=suggestion)
        if len(category_list) == 0:
            category_list = Category.objects.order_by('-likes')
        return render(request, 'rango/categories.html', {'categories': category_list})


def about(request):
    # Spoiler: now you DO need a context dictionary!
    context_dict = {}
    visitor_cookie_handler(request)
    context_dict['visits'] = request.session['visits']

    return render(request, 'rango/about.html', context=context_dict)

def index_cookie_handler(request):
    request.session['vocabulary_list'] = get_server_side_cookie(request, 'vocabulary_list', [])

def learnt(request):
    userp_id = request.GET['result_id']
    try:
        userp = UserPrevious.objects.get(id=userp_id)
    except UserPrevious.DoesNotExist:
        return HttpResponse(-1)
    except ValueError:
        return HttpResponse(-1)

    if userp.learnt >= 1:
        print('inc')
        userp.learnt = 1
        userp.learnt = userp.learnt - 1
    elif userp.learnt == 0:
        print('dec')
        userp.learnt = userp.learnt + 1
    print(userp.learnt)

    userp.save()

    return HttpResponse(userp.learnt)

def index(request, source_title_slug):
    # Query the database for a list of ALL categories currently stored.
    # Order the categories by the number of likes in descending order.
    # Retrieve the top 5 only -- or all if less than 5.
    # Place the list in our context_dict dictionary (with our boldmessage!)
    # that will be passed to the template engine.
    s = source_helper(source_title_slug)
    source_id = s['source_id']
    title = s['title']
    context_dict = {}
    context_dict['title'] = title
    category_list = Category.objects.filter(sources=source_id).order_by('-pub_date')[:10]


    def check_v_list(request):

        try:
            str(request.session['vocabulary_list'][0]['source__title'])
            return str(request.session['vocabulary_list'][0]['source__title'])!=str(source_id)
        except IndexError:
            print('true')
            return True
        except KeyError:
            print('true')
            return True
    checkvlist = check_v_list(request)
    print(checkvlist)

    if 'vocabulary_list' not in request.session or checkvlist:
        print('slow')
        vocabulary_list = list(Vocab.objects.filter(source=source_id).order_by('-hsk_lvl', '-frequency').values('category__name',
                                            'source__title', 'hsk_lvl', 'frequency', 'chinese', 'pinyin', 'english'))
        request.session['vocabulary_list'] = vocabulary_list
        print('vocabulary_list: ')
        print(str(vocabulary_list[0]['source__title'])==str(source_id))
        print(vocabulary_list[0]['source__title'], source_id)
    else:
        print('fast')
        index_cookie_handler(request)
        vocabulary_list = request.session['vocabulary_list']

    # create pages
    page = request.GET.get('page', 1)
    paginator = Paginator(vocabulary_list, 10000)
    try:
        vocabulary = paginator.page(page)
    except PageNotAnInteger:
        vocabulary = paginator.page(1)
    except EmptyPage:
        vocabulary = paginator.page(paginator.num_pages)

    context_dict['categories'] = category_list
    context_dict['vocabulary'] = vocabulary

    return render(request, 'rango/index.html', context=context_dict)

def show_category(request, category_name_slug):

    # Create a context dictionary which we can pass
    # to the template rendering engine.
    context_dict = {}
    try:
        # Can we find a category name slug with the given name?
        # If we can't, the .get() method raises a DoesNotExist exception.
        # The .get() method returns one model instance or raises an exception.
        category = Category.objects.get(slug=category_name_slug)
        # Retrieve all of the associated pages.
        # The filter() will return a list of page objects or an empty list.
        vocabulary = Vocab.objects.filter(category=category)
        # Adds our results list to the template context under name pages.
        context_dict['vocabulary'] = vocabulary
        # We also add the category object from
        # the database to the context dictionary.
        # We'll use this in the template to verify that the category exists.
        context_dict['category'] = category
    except Category.DoesNotExist:

        context_dict['category'] = None
        context_dict['vocabulary'] = None

    return render(request, 'rango/category.html', context=context_dict)


@login_required
def add_category(request):
    user = request.user
    print(user)
    s = Source.objects.get_or_create(title=user)[0]
    s.save()
    form = CategoryForm()
    # A HTTP POST?
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            newcat = form.save(commit=True)

            newcat.sources.add(s)
            newcat.save()
            # Now that the category is saved, we could confirm this.
            # For now, just redirect the user back to the index view.
            return redirect('/rango/')
        else:

            print(form.errors)

    return render(request, 'rango/add_category.html', {'form': form})


@login_required
def add_vocabulary(request, category_name_slug):
    user = request.user
    print(user)

    try:
        category = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        category = None

    # You cannot add a page to a Category that does not exist...
    if category is None:
        return redirect('/rango/')

    form = VocabForm()
    if request.method == 'POST':
        form = VocabForm(request.POST)
        hsk_id = request.POST.get('hsk_lvl', 0)
        h = HSK.objects.get_or_create(hsk_level=hsk_id)[0]
        h.save()
        s = Source.objects.get_or_create(title=user)[0]
        s.save()

        if form.is_valid():

            if category:

                word = form.save(commit=False)

                if hsk_id:
                    word.hsk = h
                word.category = category
                word.source = s
                word.save()
                return redirect(reverse('rango:show_category', kwargs={'category_name_slug': category_name_slug}))
        else:
            print(form.errors)

    context_dict = {'form': form, 'category': category}
    return render(request, 'rango/add_vocabulary.html', context=context_dict)



# def register(request):
#     registered = False
#
#     if request.method == 'POST':
#         user_form = UserForm(request.POST)
#         profile_form = UserProfileForm(request.POST)
#
#         if user_form.is_valid() and profile_form.is_valid():
#             user = user_form.save()
#             user.set_password(user.password)
#             user.save()
#
#             profile = profile_form.save(commit=False)
#             profile.user = user
#
#             if 'picture' in request.FILES:
#                 profile.picture = request.FILES['picture']
#
#             profile.save()
#             registered = True
#         else:
#             print(user_form.errors, profile_form.errors)
#     else:
#         user_form = UserForm()
#         profile_form = UserProfileForm()
#
#     return render(request, 'rango/register.html', context={'user_form': user_form, 'profile_form': profile_form, 'registered': registered})

# def user_login(request):
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#
#         user = authenticate(username=username, password=password)
#
#         if user:
#             if user.is_active:
#                 login(request, user)
#                 return redirect(reverse('rango:index'))
#             else:
#                 return HttpResponse("Your Rango account is disabled.")
#         else:
#             print(f"Invalid login details: {username}, {password}")
#             return HttpResponse("Invalid login details supplied.")
#     else:
#         return render(request, 'rango/login.html')


class ProfileView(View):
    def get_user_details(self, username):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None
        user_profile = UserProfile.objects.get_or_create(user=user)[0]
        user_profile.document.delete()
        form = UserProfileForm({'website': user_profile.website,
                                'picture': user_profile.picture,
                                'document': user_profile.document})
        return (user, user_profile, form)

    @method_decorator(login_required)
    def get(self, request, username):
        try:
            (user, user_profile, form) = self.get_user_details(username)
        except TypeError:
            return redirect(reverse('rango:index'))
        context_dict = {'user_profile': user_profile,
                        'selected_user': user,
                        'form': form}
        # x = doc_process(user)
        # y = binfile_to_db(user)
        return render(request, 'rango/profile.html', context_dict)

    @method_decorator(login_required)
    def post(self, request, username):

        try:
            (user, user_profile, form) = self.get_user_details(username)
        except TypeError:
            return redirect(reverse('rango:index'))
        form = UserProfileForm(request.POST, request.FILES, instance=user_profile)
        if form.is_valid():
            form.save(commit=True)
            if user_profile.document:
                x = doc_process(user)
                y = binfile_to_db(user)
            return redirect('rango:profile', user.username)
        else:
            print(form.errors)
        context_dict = {'user_profile': user_profile,
                        'selected_user': user,
                        'form': form}
        print("Processing document: doc_process")

        return render(request, 'rango/profile.html', context_dict)

@login_required
def register_profile(request):
    form = UserProfileForm()
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES)
        if form.is_valid():
            user_profile = form.save(commit=False)
            user_profile.user = request.user
            user_profile.save()

            username = form.cleaned_data.get('username')
            messages.success(request, f"New account created: {username}")
            login(request, request.user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect(reverse('rango:bookshelf_user'))
        else:
            print(form.errors)
            messages.error(request, "Account creation failed")
    context_dict = {'form': form}
    return render(request, 'rango/profile_registration.html', context_dict)

def some_view(request):
    if not request.user.is_authenticated():
        return HttpResponse("You are logged in.")
    else:
        return HttpResponse("You are not logged in.")


@login_required
def restricted(request):
    return render(request, 'rango/restricted.html')

def sentence_cookie_handler(request):
    request.session['sentence_list'] = get_server_side_cookie(request, 'sentence_list', [])

@login_required
def show_sentence(request):
    s = source_helper(request.user)
    context_dict = {}
    if 'sentence_list' not in request.session:
        print('slow')
        sentence_list = list(Sentence.objects.filter(source=s['source_eng_id']).values('number','chinese', 'pinyin'))

        request.session['sentence_list'] = sentence_list

    else:
        print('fast')
        sentence_cookie_handler(request)
        sentence_list = request.session['sentence_list']

    # create pages
    page = request.GET.get('page', 1)
    paginator = Paginator(sentence_list, 100)
    try:
        sentences = paginator.page(page)
    except PageNotAnInteger:
        sentences = paginator.page(1)
    except EmptyPage:
        sentences = paginator.page(paginator.num_pages)

    context_dict['sentences'] = sentences

    response = render(request, 'rango/sentence.html', context=context_dict)
    return response


def kanshu_cookie_handler(request):
    request.session['sides'] = get_server_side_cookie(request, 'sides', [])
    request.session['sentence_number'] = get_server_side_cookie(request, 'sentence_number', '')
    request.session['mother_text'] = get_server_side_cookie(request, 'mother_text', [])
    request.session['adjust'] = get_server_side_cookie(request, 'adjust', '0')
    request.session['source'] = get_server_side_cookie(request, 'source', [])
    request.session['bookmark'] = get_server_side_cookie(request, 'bookmark', [])


@login_required
def mother_text_helper_sentence(request, source_title_slug):
    context_dict = {}
    result_list = []
    sentence_number = request.session['sentence_number']
    mother_sentence_number = int(sentence_number) - 1
    s = source_helper(source_title_slug)
    if request.method == 'GET':
        mother_text = list(MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[
                           mother_sentence_number:mother_sentence_number + 100])
        request.session['mother_text'] = mother_text

    if request.method == 'POST':
        adjust = request.POST['adjust'].strip()
        if adjust:
            try:
                mother_sentence_number = int(adjust)+(int(sentence_number))
                if mother_sentence_number < 0:
                    mother_sentence_number = 0
            except ValueError:
                mother_sentence_number = int(sentence_number)

            mother_text = list(MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[mother_sentence_number:mother_sentence_number+100])

            request.session['adjust'] = adjust
            request.session['mother_text'] = mother_text

    source = Source.objects.get(slug=source_title_slug)
    context_dict['source'] = source

    context_dict['title'] = s['title']
    context_dict['mother_text'] = request.session['mother_text']
    context_dict['sides'] = request.session['sides']
    context_dict['sentence_number'] = request.session['sentence_number']
    context_dict['adjust'] = request.session['adjust']
    return render(request, 'rango/kanshu_sentence2.html', context=context_dict)


@login_required
def mother_text_helper(request, source_title_slug):
    context_dict = {}
    result_list = []
    sentence_number = request.session['sentence_number']
    mother_sentence_number = int(sentence_number) - 1
    s = source_helper(source_title_slug)
    if request.method == 'GET':
        mother_text = list(MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[
                           mother_sentence_number:mother_sentence_number + 100])
        request.session['mother_text'] = mother_text

    if request.method == 'POST':
        adjust = request.POST['adjust'].strip()
        if adjust:
            try:
                mother_sentence_number = int(adjust)+(int(sentence_number))
                if mother_sentence_number < 0:
                    mother_sentence_number = 0
            except ValueError:
                mother_sentence_number = int(sentence_number)

            mother_text = list(MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[mother_sentence_number:mother_sentence_number+100])

            request.session['adjust'] = adjust
            request.session['mother_text'] = mother_text

    source = Source.objects.get(slug=source_title_slug)
    context_dict['source'] = source
    context_dict['title'] = s['title']
    context_dict['mother_text'] = request.session['mother_text']
    context_dict['sides'] = request.session['sides']
    context_dict['sentence_number'] = request.session['sentence_number']
    context_dict['adjust'] = request.session['adjust']
    return render(request, 'rango/kanshu_carousel2.html', context=context_dict)


def source_helper(source_title_slug):

    source_info = {}

    if str(source_title_slug) == 'harry_potter_mandarin':
        source = 'Harry_Potter_Mandarin'
        source_eng = 'Harry_Potter_English'
        title = 'Harry Potter 哈利波特'
        language = "Mandarin"
        type = title
        author = "J. K. Rowling"
        about_author = "Joanne Rowling, born 31 July 1965, better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer, and screenwriter. She is best known for writing the Harry Potter fantasy series, which has won multiple awards and sold more than 500 million copies, becoming the best-selling book series in history."
        about_book = "Harry Potter and the Philosopher's Stone is a fantasy novel written by British author J. K. Rowling. The first novel in the Harry Potter series and Rowling's debut novel, it follows Harry Potter, a young wizard who discovers his magical heritage on his eleventh birthday, when he receives a letter of acceptance to Hogwarts School of Witchcraft and Wizardry. Harry makes close friends and a few enemies during his first year at the school, and with the help of his friends, he faces an attempted comeback by the dark wizard Lord Voldemort, who killed Harry's parents, but failed to kill Harry when he was just 15 months old."
        url = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher%27s_Stone"

        print(source)

        # # comment out after first run!
        # s = Source.objects.get_or_create(title=source, about_author=about_author, about_book=about_book,
        #                                  language=language, author=author, type=type, url=url)
        # s.save()

        # keep after first run
        source_id = Source.objects.get_or_create(title=source)[0]
        source_id.title = source
        source_id.about_author = about_author
        source_id.about_book = about_book
        source_id.language = language
        source_id.author = author
        source_id.type = type
        source_id.url = url
        source_id.save()
        source_eng_id = Source.objects.get_or_create(title=source_eng)

    elif str(source_title_slug) == 'the_little_prince_mandarin':
        source = 'The_Little_Prince_Mandarin'
        source_eng = 'The_Little_Prince_English'
        title = 'The Little Prince 小王子'
        language = "Mandarin"
        type = title
        author = "Antoine"
        print(source)

        # # comment out after first run!
        # s = Source.objects.get_or_create(title=source, about_author=about_author, about_book=about_book,
        #                                  language=language, author=author, type=type, url=url)
        # s.save()

        # keep after first run
        source_id = Source.objects.get_or_create(title=source)[0]
        source_id.title=source
        source_id.about_author = about_author
        source_id.about_book = about_book
        source_id.language = language
        source_id.author = author
        source_id.type = type
        source_id.url = url
        source_id.save()
        source_eng_id = Source.objects.get_or_create(title=source_eng)

    elif str(source_title_slug) == 'brothers_mandarin':
        source = 'Brothers_Mandarin'
        source_eng = 'Brothers_English'
        language = "Mandarin"
        title = 'Brothers 兄弟'
        type = title
        author = "Yu Hua 余华"
        about_author = "Yu Hua (simplified Chinese: 余华; traditional Chinese: 余華; pinyin: Yú Huá) is a Chinese author, born April 3, 1960 in Hangzhou, Zhejiang province. Shortly after his debut as a fiction writer in 1983, Yu Hua was regarded as a promising avant-garde or post-New Wave writer. Many critics also regard him as a champion for Chinese meta-fictional or postmodernist writing. His novels To Live (1993) and Chronicle of a Blood Merchant (1995) were widely acclaimed. When he experimented with more chaotic themes like in Brothers (2005–06), Yu Hua received criticism from critics and readers."
        about_book = "Brothers is the longest novel written by the Chinese novelist Yu Hua, comprised of 76 chapters, separately published in 2005 for the part 1 (of the first 26 chapters) and in 2006 for part 2 (of the rest 50 chapters) by Shanghai Literature and Art Publishing House. The story of the novel revolves around the step-brotherhood between Baldy Li and Song Gang who are the protagonists. Their life that has been of absurd and tragedy throughout China's history from the 1960s to the early period of Chinese Economic Reform serves for the main plot. The division of the novel into two parts is contextually on the basis of the death of Li Lan who is the biological mother of Baldy Li and the step-mother of Song Gang. The first part mainly focuses on their life of childhood, especially during the Cultural Revolution period with the collapse of their reorganized family and the tragedy of Song Fanping who is the biological father of Song Gang and the Step-father of Baldy Li. The second part mainly focuses on their life of adulthood with different living trajectories, especially during the early period of Chinese Economic Reform that is of the mixture of absurd and tragedy."

        url = "https://en.wikipedia.org/wiki/Brothers_(Yu_novel)"
        print(source)

        # # comment out after first run!
        # s = Source.objects.get_or_create(title=source, about_author=about_author, about_book=about_book,
        #                                  language=language, author=author, type=type, url=url)
        # s.save()

        # keep after first run
        source_id = Source.objects.get_or_create(title=source)[0]
        source_id.title=source
        source_id.about_author = about_author
        source_id.about_book = about_book
        source_id.language = language
        source_id.author = author
        source_id.type = type
        source_id.url = url
        source_id.save()
        source_eng_id = Source.objects.get_or_create(title=source_eng)

    elif str(source_title_slug) == 'three_body_problem_mandarin':
        source = 'Three_Body_Problem_Mandarin'
        source_eng = 'Three_Body_Problem_English'
        language = "Mandarin"
        title = 'Three body problem 三体'
        type = title
        author = "Liu Cixin 刘慈欣"
        about_author = "Liu Cixin (Chinese: 刘慈欣, pronounced [ljǒu tsʰɨ̌ɕín], l'yoh tsih-shin; born 23 June 1963) is a prominent Chinese science fiction writer. Liu's parents worked in a mine in Shanxi. Due to the violence of the Cultural Revolution he was sent to live in his ancestral home in Luoshan County, Henan. Liu graduated from the North China University of Water Conservancy and Electric Power in 1988. He then worked as a computer engineer at a power plant in Shanxi province. He is a nine-time winner of China's Galaxy Award and has also received the 2015 Hugo Award for his novel The Three-Body Problem as well as the 2017 Locus Award for Death's End. He has also been nominated for the Nebula Award."
        about_book = "The Three-Body Problem (Chinese: 三体; lit. 'Three-Body'; pinyin: sān tǐ) is a science fiction novel by the Chinese writer Liu Cixin. The title refers to the three-body problem in orbital mechanics. It is the first novel of the Remembrance of Earth's Past (Chinese: 地球往事) trilogy, but Chinese readers generally refer to the whole series as The Three-Body Problem. The second and third novels in the trilogy are The Dark Forest and Death's End.Set against the backdrop of China's Cultural Revolution, a secret military project sends signals into space to establish contact with aliens. An alien civilization on the brink of destruction captures the signal and plans to invade Earth. Meanwhile, on Earth, different camps start forming, planning to either welcome the superior beings and help them take over a world seen as corrupt, or to fight against the invasion."
        url = "https://en.wikipedia.org/wiki/The_Three-Body_Problem_(novel)"
        print(source)

        # # comment out after first run!
        # s = Source.objects.get_or_create(title=source, about_author=about_author, about_book=about_book,
        #                                  language=language, author=author, type=type, url=url)
        # s.save()

        # keep after first run
        source_id = Source.objects.get_or_create(title=source)[0]
        source_id.title = source
        source_id.about_author = about_author
        source_id.about_book = about_book
        source_id.language = language
        source_id.author = author
        source_id.type = type
        source_id.url = url
        source_id.save()
        source_eng_id = Source.objects.get_or_create(title=source_eng)
    elif str(source_title_slug) == 'do_androids_dream_of_electric_sheep_mandarin':
        source = 'Do_Androids_Dream_Of_Electric_Sheep_Mandarin'
        source_eng = 'Do_Androids_Dream_Of_Electric_Sheep_English'
        title = 'Do Androids Dream of Electric Sheep 银翼杀手'
        type = title
        author = "Philip Kindred Dick"
        language = "Mandarin"
        about_author = "Philip Kindred Dick (December 16, 1928 – March 2, 1982) was an American writer known for his work in science fiction. He wrote 44 published novels and approximately 121 short stories, most of which appeared in science fiction magazines during his lifetime. His fiction explored varied philosophical and social themes, and featured recurrent elements such as alternate realities, simulacra, monopolistic corporations, drug abuse, authoritarian governments, and altered states of consciousness. His work was concerned with questions surrounding the nature of reality, perception, human nature, and identity."
        about_book = "The plot of Electric Sheep follows the story of detective Rick Deckard in a post-apocalyptic San Francisco as he tracks down runaway androids (who as slaves are forbidden to come back to Earth from the off world colonies), deals with his Virtual Reality-addicted wife, and keeps up the pretence that his electric sheep is in fact real."
        url = "https://en.wikipedia.org/wiki/Do_Androids_Dream_of_Electric_Sheep%3F"
        print(source)

        # # comment out after first run!
        # s = Source.objects.get_or_create(title=source, about_author=about_author, about_book=about_book,
        #                                  language=language, author=author, type=type, url=url)
        # s.save()

        # keep after first run
        source_id = Source.objects.get_or_create(title=source)[0]
        source_id.title=source
        source_id.about_author = about_author
        source_id.about_book = about_book
        source_id.language = language
        source_id.author = author
        source_id.type = type
        source_id.url = url
        source_id.save()
        source_eng_id = Source.objects.get_or_create(title=source_eng)


    source_info = {'source_id': source_id, 'title': title, 'source_eng': source_eng, 'source_eng_id': source_eng_id}
    return source_info


@login_required
def kanshu_dashboard(request):

    context_dict = {}

    if 'userdict_list' not in request.session:
        print('slow')
        userdict_list = list(UserDictionary.objects.filter(user=request.user).order_by('-hsk', '-frequency').values('category',
                                            'source', 'hsk', 'frequency', 'chinese', 'pinyin', 'english'))
        request.session['userdict_list'] = userdict_list

    else:
        print('fast')
        index_cookie_handler(request)
        userdict_list = request.session['userdict_list']

    # create pages
    page = request.GET.get('page', 1)
    paginator = Paginator(userdict_list, 10000)
    try:
        vocabulary = paginator.page(page)
    except PageNotAnInteger:
        vocabulary = paginator.page(1)
    except EmptyPage:
        vocabulary = paginator.page(paginator.num_pages)

    context_dict['vocabulary'] = vocabulary

    return render(request, 'rango/kanshu_dashboard.html', context=context_dict)


def words_in_sent(request):
    context_dict = {}
    word_to_match = request.GET['word']
    source = request.GET['source']
    s = source_helper(source)
    print(s, source)
    print(word_to_match)
    sentences = Sentence.objects.filter().values('chinese')

    sentence_w_word = []
    for sentence in sentences:
        if word_to_match in sentence['chinese']:
            sentence_w_word.append(sentence['chinese'])

    context_dict['sample_sentences'] = sentence_w_word



    results = list(Sentence.objects.filter(source=s['source_id']).values('number', 'chinese', 'pinyin', 'english'))
    vlist = list(Vocab.objects.filter(source=s['source_id']).values('chinese', 'pinyin', 'english', 'frequency'))

    side_word_lists = {}
    sentence_number = 0

    for result in results:
        if word_to_match in result['chinese']:
            t_original = result['chinese']
            sentence_vocab = t_original.split("\t")
            sentence_number = sentence_number + 1
            side_word_list = []
            for word in sentence_vocab:

                try:

                    vocab_index = next(item for item in vlist if item['chinese'] == word)
                    english = vocab_index['english']
                    chinese = vocab_index['chinese']
                    pinyin = vocab_index['pinyin']
                    frequency = vocab_index['frequency']

                    side_word_dict = {'pinyin': pinyin, 'english': english, 'chinese': chinese,
                                      'frequency': frequency, 'end': 0}

                except StopIteration:

                    side_word_dict = {'pinyin': word, 'english': word, 'chinese': word, 'frequency': 0, 'end': 0}
                side_word_list.append(side_word_dict)
            side_word_dict = {'pinyin': '。', 'english': '。', 'chinese': '。', 'frequency': 0, 'end': 1}
            side_word_list.append(side_word_dict)
            # print(side_word_list)
            side_word_lists[str(sentence_number)] = side_word_list



    html = render_to_string('rango/word_in_sentence.html', {'source': s['source_id'], 'side_word_lists': side_word_lists, 'sentence_w_word': sentence_w_word, 'sides': side_word_list, 'result_list': results})

    return HttpResponse(html)



@login_required
def kanshu_carousel(request, source_title_slug):

    context_dict = {}
    print(source_title_slug)
    s = source_helper(source_title_slug)

    user = request.user
    chapter_list = list(Chapter.objects.filter(source=s['source_id']).values('start_sentence_num', 'chapter_title', 'chapter_num', 'section_title', 'section_num'))
    # print(chapter_list)
    result_list = []
    mother_text = []


    if request.method == 'GET':
        b = UserBookmark.objects.get_or_create(user=user, source=s['source_id'])[0]
        print(b.bookmark)

        sentence_number = b.bookmark

        if sentence_number:

            r = run_kanshu(sentence_number, user, s['source_id'])
            context_dict['result_list'] = r['results']
            request.session['sides'] = r['sides']
            request.session['sentence_number'] = sentence_number
            request.session['bookmark'] = sentence_number
            try:
                adjust = int(request.session['adjust'])
            except KeyError:
                adjust = 0
            except ValueError:
                adjust = 0

            start = int(sentence_number)-1+adjust
            if start < 0:
                start = 0
            end = start+100

            try:
                mother_text = list(
                    MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[start:end])
                request.session['mother_text'] = mother_text
            except:
                mother_text = []
                request.session['mother_text'] = mother_text

    if request.method == 'POST':

        sentence_number = request.POST['sentence_number'].strip()

        if sentence_number:

            r = run_kanshu(sentence_number, user, s['source_id'])
            context_dict['result_list'] = r['results']
            request.session['sides'] = r['sides']
            request.session['sentence_number'] = sentence_number
            request.session['bookmark'] = sentence_number
            try:

                adjust = int(request.session['adjust'])


            except ValueError:
                adjust = 0

            start = int(sentence_number)-1+adjust
            if start < 0:
                start = 0
            end = start+100

            try:
                mother_text = list(
                    MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[start:end])
                request.session['mother_text'] = mother_text
            except:
                mother_text = []
                request.session['mother_text'] = mother_text



    kanshu_cookie_handler(request)
    source = Source.objects.get(slug=source_title_slug)
    context_dict['chapter_list'] = chapter_list
    context_dict['source'] = source
    context_dict['sides'] = request.session['sides']
    context_dict['sentence_number'] = request.session['sentence_number']
    context_dict['mother_text'] = request.session['mother_text']
    context_dict['adjust'] = request.session['adjust']
    context_dict['title'] = s['title']
    context_dict['bookmark'] = request.session['bookmark']

    return render(request, 'rango/kanshu_carousel.html', context=context_dict)


@login_required
def kanshu_sentence(request, source_title_slug):
    context_dict = {}

    s = source_helper(source_title_slug)

    user = request.user
    chapter_list = list(Chapter.objects.filter(source=s['source_id']).values('start_sentence_num', 'chapter_title', 'chapter_num', 'section_title', 'section_num'))

    result_list = []



    if request.method == 'GET':
        b = UserBookmark.objects.get_or_create(user=user, source=s['source_id'])[0]


        sentence_number = b.bookmark

        if sentence_number:

            r = run_kanshu(sentence_number, user, s['source_id'])
            context_dict['result_list'] = r['results']
            request.session['sides'] = r['sides']
            request.session['sentence_number'] = sentence_number
            request.session['bookmark'] = sentence_number
            try:
                adjust = int(request.session['adjust'])
            except KeyError:
                adjust = 0
            except ValueError:
                adjust = 0

            start = int(sentence_number)-1+adjust
            if start < 0:
                start = 0
            end = start+100
            try:
                mother_text = list(MotherText.objects.filter(source=s['source_eng_id']).values('mother', 'number')[start:end])
                request.session['mother_text'] = mother_text
            except:
                mother_text = []
                request.session['mother_text'] = mother_text
    if request.method == 'POST':

        sentence_number = request.POST['sentence_number'].strip()

        if sentence_number:

            r = run_kanshu(sentence_number, user, s['source_id'])
            context_dict['result_list'] = r['results']
            request.session['sides'] = r['sides']
            request.session['sentence_number'] = sentence_number
            request.session['bookmark'] = sentence_number
            try:

                adjust = int(request.session['adjust'])

            except ValueError:
                adjust = 0

            start = int(sentence_number)-1+adjust
            if start < 0:
                start = 0
            end = start+100

            try:
                mother_text = list(MotherText.objects.filter(source=s['source_id']).values('mother', 'number')[start:end])
                request.session['mother_text'] = mother_text
            except:
                mother_text = []
                request.session['mother_text'] = mother_text

    kanshu_cookie_handler(request)
    source = Source.objects.get(slug=source_title_slug)
    context_dict['chapter_list'] = chapter_list
    context_dict['source'] = source
    context_dict['sides'] = request.session['sides']
    context_dict['sentence_number'] = request.session['sentence_number']
    context_dict['mother_text'] = request.session['mother_text']
    context_dict['adjust'] = request.session['adjust']
    context_dict['title'] = s['title']
    context_dict['bookmark'] = request.session['bookmark']
    return render(request, 'rango/kanshu_sentence.html', context=context_dict)


def visitor_cookie_handler(request):
    visits = int(get_server_side_cookie(request, 'visits', '1'))
    last_visit_cookie = get_server_side_cookie(request, 'last_visit', str(datetime.now()))
    last_visit_time = datetime.strptime(last_visit_cookie[:-7], '%Y-%m-%d %H:%M:%S')

    if (datetime.now() - last_visit_time).days > 0:
        visits = visits + 1
        request.session['last_visit'] = str(datetime.now())
    else:
        request.session['last_visit'] = last_visit_cookie

    request.session['visits'] = visits


def get_server_side_cookie(request, cookie, default_val=None):
    val = request.session.get(cookie)
    if not val:
        val = default_val
    return val


# class LikeCategoryView(View):
#
#     @method_decorator(login_required)
#     def get(self, request):


def like_category(request):

    category_id = request.GET['category_id']
    try:
        category = Category.objects.get(id=int(category_id))
    except Category.DoesNotExist:
        return HttpResponse(-1)
    except ValueError:
        return HttpResponse(-1)

    category.likes = category.likes + 1
    category.save()

    return HttpResponse(category.likes)


def bookmark(request):

    user = request.user
    bookmark = request.GET['bookmark']
    # adjust = request.GET['adjust']
    source_title_slug = request.GET['source_title_slug']

    try:
        s = source_helper(source_title_slug)

        b = UserBookmark.objects.get_or_create(user=user, source=s['source_id'])[0]
        b.bookmark = bookmark
        b.save()

    except UserBookmark.DoesNotExist:
        return HttpResponse(-1)
    except ValueError:
        return HttpResponse(-1)

    return HttpResponse(b.bookmark)


@login_required
def del_word2dict(request):
    user = request.user
    chinese = request.GET['chinese']
    pinyin = request.GET['pinyin']
    english = request.GET['english']
    source_title_slug = request.GET['source_title_slug']

    delete_session(request, 'userdict_list')

    s = source_helper(source_title_slug)
    print("source_title_slug: ", s['source_id'])

    v = UserDictionary.objects.get_or_create(user=user, chinese=chinese, pinyin=pinyin, english=english, source=source_title_slug)[0].delete()
    # messages.success(request, "Word removed.")
    return HttpResponse(-1)


@login_required
def add_word2dict(request):
    user = request.user
    chinese = request.GET['chinese']
    pinyin = request.GET['pinyin']
    english = request.GET['english']
    source_title_slug = request.GET['source_title_slug']

    delete_session(request, 'userdict_list')

    s = source_helper(source_title_slug)
    print("source_title_slug: ", s['source_id'])

    v = UserDictionary.objects.get_or_create(user=user, chinese=chinese, pinyin=pinyin, english=english, source=source_title_slug)[0]

    word = Vocab.objects.filter(chinese=chinese, source=s['source_id'])[0]

    v.category = word.category.name
    v.hsk = word.hsk_lvl
    v.frequency = word.frequency
    v.save()

    print("word: ", word.hsk, word.frequency, word.category.name)
    return HttpResponse(word)
    # try:
    #     category = Category.objects.get(slug=category_name_slug)
    # except Category.DoesNotExist:
    #     category = None
    #
    # # You cannot add a page to a Category that does not exist...
    # if category is None:
    #     return redirect('/rango/')
    #
    # form = VocabForm()
    # if request.method == 'POST':
    #     form = VocabForm(request.POST)
    #     hsk_id = request.POST.get('hsk_lvl', 0)
    #     h = HSK.objects.get_or_create(hsk_level=hsk_id)[0]
    #     h.save()
    #     s = Source.objects.get_or_create(title=user)[0]
    #     s.save()
    #
    #     if form.is_valid():
    #
    #         if category:
    #
    #             word = form.save(commit=False)
    #
    #             if hsk_id:
    #                 word.hsk = h
    #             word.category = category
    #             word.source = s
    #             word.save()
    #             return redirect(reverse('rango:show_category', kwargs={'category_name_slug': category_name_slug}))
    #     else:
    #         print(form.errors)
    #
    # context_dict = {'form': form, 'category': category}
    # return render(request, 'rango/add_vocabulary.html', context=context_dict)

# def next_kanshu(request):
#     print('nextttt')
#     sentence_id = request.GET['sentence_id']
#     next = int(sentence_id) + 1
#     return redirect(reverse('rango:kanshu', next))
#
#     # return HttpResponse(category.likes)