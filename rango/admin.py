

# Register your models here.
from django.contrib import admin
from rango.models import Chapter, About, UserDictionary, UserBookmark, Category, Vocab, Sentence, HSK, MotherText, Source, UserCategory, UserVocab, UserSentence, UserHSK, UserMotherText, UserSource, UserPrevious
from rango.models import UserProfile

class AboutAdmin(admin.ModelAdmin):
    list_display = ('about',)

class UserPreviousAdmin(admin.ModelAdmin):
    list_display = ('user', 'previous', 'chinese', 'updated',)

class UserBookmarkAdmin(admin.ModelAdmin):
    list_display = ('user', 'source', 'bookmark',)

class VocabAdmin(admin.ModelAdmin):
    list_display = ('source', 'hsk', 'category', 'chinese',  'pinyin', 'english', 'frequency', 'score', 'learnt')

class UserDictionaryAdmin(admin.ModelAdmin):
    list_display = ('user', 'source', 'hsk', 'category', 'chinese',  'pinyin', 'english', 'frequency', 'updated')

class UserVocabAdmin(admin.ModelAdmin):
    list_display = ('source', 'hsk', 'user', 'category', 'chinese',  'pinyin', 'english', 'frequency', 'score', 'learnt')
    prepopulated_fields = {'slug':('pinyin',)}
# Add in this class to customise the Admin Interface
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('get_sources', 'name', 'pub_date', 'likes')
    prepopulated_fields = {'slug':('name',)}
class UserCategoryAdmin(admin.ModelAdmin):
    list_display = ('get_sources', 'name', 'user')
    prepopulated_fields = {'slug':('name',)}

class HSKAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('hsk_level',)}
class UserHSKAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('hsk_level',)}

class ChapterAdmin(admin.ModelAdmin):
    list_display = ('source','chapter_num','chapter_title','section_num','section_title',)


class SentenceAdmin(admin.ModelAdmin):
    list_display = ('source', 'chinese',  'pinyin', 'english', 'number', 'pub_date')
    prepopulated_fields = {'slug': ('pinyin',)}

class UserSentenceAdmin(admin.ModelAdmin):
    list_display = ('source', 'chinese',  'pinyin', 'english', 'number', 'pub_date')
    prepopulated_fields = {'slug': ('pinyin',)}

class MotherTextAdmin(admin.ModelAdmin):
    list_display = ('source', 'mother', 'number', 'pub_date')
    prepopulated_fields = {'slug': ('number',)}
class UserMotherTextAdmin(admin.ModelAdmin):
    list_display = ('source', 'mother', 'number', 'pub_date')
    prepopulated_fields = {'slug': ('number',)}

class UserSourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'author', 'type')
    prepopulated_fields = {'slug': ('title',)}

class SourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'type', 'about_book', 'about_author', 'language',)
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(About, AboutAdmin)
admin.site.register(UserDictionary, UserDictionaryAdmin)
admin.site.register(UserBookmark, UserBookmarkAdmin)
admin.site.register(UserPrevious, UserPreviousAdmin)
admin.site.register(UserSource, UserSourceAdmin)
admin.site.register(MotherText, MotherTextAdmin)
admin.site.register(HSK, HSKAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Vocab, VocabAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(UserProfile)
admin.site.register(Sentence, SentenceAdmin)
admin.site.register(UserMotherText, UserMotherTextAdmin)
admin.site.register(UserHSK, UserHSKAdmin)
admin.site.register(UserCategory, UserCategoryAdmin)
admin.site.register(UserVocab, UserVocabAdmin)
admin.site.register(UserSentence, UserSentenceAdmin)
admin.site.register(Chapter, ChapterAdmin)




