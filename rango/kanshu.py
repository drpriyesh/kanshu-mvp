import json
import requests
from rango.models import Category, Vocab, Sentence, UserPrevious
from django.db.models import F
from django.contrib.auth.models import User
from googletrans import Translator
import re
from datetime import datetime, timezone
import time


def add_userprevious(user, chinese):
    p = UserPrevious.objects.get_or_create(user=user, chinese=chinese)[0]
    p.save()
    return p

# just return chinese and pinyin without english tran?
def run_kanshu(sentence_number, user, source):

    original_sentence_number = sentence_number
    sentence_number = int(sentence_number)

    results = list(Sentence.objects.filter(source=source).values('number', 'chinese', 'pinyin', 'english')[sentence_number-1:sentence_number+15])

    side_word_list = []

    vlist = list(Vocab.objects.filter(source=source).values('chinese', 'pinyin', 'english', 'frequency'))

    plist = list(UserPrevious.objects.filter(user=user).values('id', 'user', 'chinese', 'previous', 'updated', 'learnt', 'slug'))

    # start_time = time.time()

    update_list = []
    create_list = []
    count = 0
    for result in results:

        t_original = result['chinese']
        # print("result['chinese']: ", t_original)
        chapter_check = "".join(t_original.split())
        # print('chapter_check: ', chapter_check)
        chapter_pattern = "^S(\d+)\((.*)\)C(\d+)\((.*)\)"
        chapter_search = re.search(chapter_pattern, chapter_check, flags=0)
        if chapter_search:
            if not chapter_search.group(1):
                t_original = ('Chapter ' + chapter_search.group(3) + '\t' + chapter_search.group(4))
                # print('t_original: ', t_original)
            else:
                t_original = ('Section ' + chapter_search.group(1) + '\t' + chapter_search.group(2)
                + '\tChapter ' + chapter_search.group(3) + '\t' + chapter_search.group(4))
                # print('t_original: ', t_original)
        sentence_vocab = t_original.split("\t")
        # print("sentence_vocab: ", sentence_vocab)


        for word in sentence_vocab:

            try:
                now = datetime.now(timezone.utc)
                # new
                vocab_index = next(item for item in vlist if item['chinese'] == word)
                english = vocab_index['english']
                chinese = vocab_index['chinese']
                pinyin = vocab_index['pinyin']
                frequency = vocab_index['frequency']
                try:
                    pdict = next(item for item in plist if item['chinese'] == word)

                    updated_time = pdict['updated']
                    if (now - updated_time).seconds > 5: #and (now - updated_time).days <= 5:
                        # print('prev + 1')
                        previous = pdict['previous'] + 1
                    elif (now - updated_time).days > 5:
                        previous = 0
                    else:
                        # print('prev + 0')
                        previous = pdict['previous']
                    id = pdict['id']
                    learnt = pdict['learnt']
                    pchinese = pdict['chinese']
                    pid = pdict['user']
                    update_dict = {'id': pid, 'chinese': pchinese, 'previous': previous, 'updated': now,
                                   'learnt': learnt}
                    update_list.append(update_dict)

                except StopIteration:
                    previous = 0
                    learnt = 1
                    id = ''
                    create_dict = {'chinese': chinese, 'user': user, 'learnt':learnt}
                    create_list.append(create_dict)

                side_word_dict = {'num': count, 'pinyin': pinyin, 'english': english, 'chinese': chinese,
                                  'sentence_number': original_sentence_number,
                                  'frequency': frequency, 'previous': previous,
                                  'learnt':learnt, 'id':  id}
                count = count + 1
            except StopIteration:

                side_word_dict = {'num': count, 'pinyin': word, 'english': word, 'chinese': word,
                                  'sentence_number': original_sentence_number, 'frequency': 0, 'learnt': 1, 'id':''}
                count = count + 1

            side_word_list.append(side_word_dict)


        side_word_dict = {'num': count,  'pinyin': '。', 'english': '。', 'chinese': '。',
                          'sentence_number': '。', 'frequency': 0, 'learnt': 1, 'id':''}
        count = count + 1
        side_word_list.append(side_word_dict)


    if update_list:
        UserPrevious.objects.bulk_update([UserPrevious(**kv) for kv in update_list], ['previous'])

    if create_list:
        UserPrevious.objects.bulk_create([UserPrevious(**q) for q in create_list])

    results = {'results': results, 'sides': side_word_list}
    # print("Part B took: ", time.time() - start_time, "to run")
    return results
