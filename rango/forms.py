from django import forms
from rango.models import Chapter, Category, Vocab, Sentence, HSK, MotherText, UserCategory, UserVocab, UserSentence, UserHSK, UserMotherText, UserSource
import datetime
from django.contrib.auth.models import User
from rango.models import UserProfile
from django import forms


class ChapterForm(forms.ModelForm):


    chapter_title = forms.CharField(widget=forms.HiddenInput(), required=False)
    # An inline class to provide
    class Meta:
        # Provide an association
        model = Chapter
        fields = ('chapter_title',)

class HSKForm(forms.ModelForm):

    hsk_level = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    # An inline class to provide
    class Meta:
        # Provide an association
        model = HSK
        fields = ('hsk_level',)

class UserHSKForm(forms.ModelForm):

    hsk_level = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    # An inline class to provide
    class Meta:
        # Provide an association
        model = UserHSK
        fields = ('hsk_level',)


class CategoryForm(forms.ModelForm):

    name = forms.CharField(widget=forms.TextInput(), max_length=Category.NAME_MAX_LENGTH, help_text="Please enter the"
    " category name. Note your category will be public! If it gets a lot of likes it will be featured in the side bar!")
    pub_date = forms.DateTimeField(initial=datetime.datetime.now, widget=forms.HiddenInput())
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    sources = forms.CharField(widget=forms.HiddenInput(), required=False)
    # An inline class to provide
    class Meta:
        # Provide an association
        model = Category
        fields = ('name',)

class UserCategoryForm(forms.ModelForm):

    name = forms.CharField(widget=forms.TextInput(), max_length=Category.NAME_MAX_LENGTH, help_text="Please enter the category name.")
    pub_date = forms.DateTimeField(initial=datetime.datetime.now, widget=forms.HiddenInput())
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)

    # An inline class to provide
    class Meta:
        # Provide an association
        model = UserCategory
        fields = ('name',)

class VocabForm(forms.ModelForm):

    hsk = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    chinese = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the chinese characters.")
    english = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the english translation.")
    pinyin = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the pinyin.")
    frequency = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    previous = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    score = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    learnt = forms.BooleanField(required=False, help_text="Please tick box if word learnt or not.")
    hsk_lvl = forms.IntegerField(initial=0, help_text="Please enter HSK level (1-6 or 0 if none)")
    # CHOICES = [('0', 'unlearnt'), ('1', 'learnt')]
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:

        model = Vocab

        exclude = ('category', 'source')

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        # If url is not empty and doesn't start with 'http://', # then prepend 'http://'.
        if url and not url.startswith('http://'):
            url = f'http://{url}'
            cleaned_data['url'] = url
        return cleaned_data


class UserVocabForm(forms.ModelForm):

    chinese = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the chinese characters.")
    english = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the english translation.")
    pinyin = forms.CharField(required=False, widget=forms.HiddenInput(), max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the pinyin.")
    frequency = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    previous = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    score = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    learnt = forms.BooleanField(required=False, help_text="Please tick box if word learnt or not.")
    hsk_lvl = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    # CHOICES = [('0', 'unlearnt'), ('1', 'learnt')]
    # learnt = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)


    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('current_user', None)
        super(UserVocabForm, self).__init__(*args, **kwargs)
        if current_user:
            self.fields['source_id'].queryset = self.fields['source_id'].queryset.filter(user=current_user.id)

    class Meta:

        model = UserVocab
        exclude = ('user', 'score', ' hsk_lvl', 'slug', 'pinyin')

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        # If url is not empty and doesn't start with 'http://', # then prepend 'http://'.
        if url and not url.startswith('http://'):
            url = f'http://{url}'
            cleaned_data['url'] = url
        return cleaned_data


class SentenceForm(forms.ModelForm):

    chinese = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")
    pinyin = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")
    english = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")

    # An inline class to provide
    class Meta:
        # Provide an association
        model = Sentence
        fields = ('chinese', 'pinyin', 'english',)

class UserSentenceForm(forms.ModelForm):

    chinese = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")
    pinyin = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")
    english = forms.CharField(widget=forms.Textarea, help_text="Please enter the chinese characters.")

    # An inline class to provide
    class Meta:
        # Provide an association
        model = UserSentence
        fields = ('chinese', 'pinyin', 'english',)


class MotherTextForm(forms.ModelForm):
    mother = forms.CharField(widget=forms.Textarea, help_text="Please enter the mother language text.")
    number = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    pub_date = forms.DateTimeField(initial=datetime.datetime.now, widget=forms.HiddenInput())

    # An inline class to provide
    class Meta:
        # Provide an association
        model = MotherText
        fields = ('mother',)

class UserMotherTextForm(forms.ModelForm):
    mother = forms.CharField(widget=forms.Textarea, help_text="Please enter the mother language text.")
    number = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    pub_date = forms.DateTimeField(initial=datetime.datetime.now, widget=forms.HiddenInput())

    # An inline class to provide
    class Meta:
        # Provide an association
        model = UserMotherText
        fields = ('mother',)

class UserSource(forms.ModelForm):
    title = forms.CharField(widget=forms.Textarea, help_text="Please enter the sources title.")

    # An inline class to provide
    class Meta:
        # Provide an association
        model = UserSource
        fields = ('title',)


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password',)

class UserProfileForm(forms.ModelForm):
    website = forms.URLField(help_text="Add a website link if you like.", required=False)
    picture = forms.ImageField(help_text="Upload a handsome/pretty profile picture if you want to!", required=False)


    class Meta:
        model = UserProfile
        fields = ['sex', ]
    # document = forms.FileField(help_text="Upload a document that you want Kanshu'd!. Then head over to the Personal Kanshu page to view them.", required=False)

    class Meta:
        model = UserProfile
        fields = ('website', 'picture',)
        # fields = ('website', 'picture', 'document',)

    def clean(self):
        cleaned_data = self.cleaned_data
        website = cleaned_data.get('website')

        # If url is not empty and doesn't start with 'http://', # then prepend 'http://'.
        if website and not website.startswith('http://'):
            website = f'http://{website}'
            cleaned_data['website'] = website
        return cleaned_data


class UserUploadForm(forms.ModelForm):
    # website = forms.URLField(widget=forms.HiddenInput(), required=False)
    # picture = forms.ImageField(widget=forms.HiddenInput(), required=False)
    document = forms.FileField(required=False)

    class Meta:
        model = UserProfile
        fields = ( 'document',)

class ContactForm(forms.Form):

    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea, initial="For example: Hey Kanshu Team! Can you find and add the following book: XXXXX.", help_text="All comments are welcome!")

class EditForm(forms.Form):

    word = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH,help_text="Please enter the chinese characters.")
    english = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please enter the english translation.")
    tag = forms.CharField(max_length=Vocab.TITLE_MAX_LENGTH, help_text="Please give this word a tag that represents what it should be e.g. 'Name' or 'Adjective'.", initial='Name', required=False)

class UserEditForm(forms.Form):


    # word = forms.CharField(max_length=UserVocab.TITLE_MAX_LENGTH, help_text="Please enter the chinese characters.")
    # english = forms.CharField(max_length=UserVocab.TITLE_MAX_LENGTH, help_text="Please enter the english translation.")
    # tag = forms.CharField(max_length=UserVocab.TITLE_MAX_LENGTH, help_text="Please give this word a tag e.g. 'Name'.", initial='Name', required=False)
    class Meta:

        model = UserVocab
        fields = ['source','chinese','english']
        exclude = ('category',)