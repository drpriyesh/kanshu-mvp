from rango.models import About

def get_about(request):
    about = About.objects.get_or_create()[0]

    return {'get_about': about}