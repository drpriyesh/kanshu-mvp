from rango.models import UserCategory, UserVocab, UserHSK, UserProfile, UserSource, UserSentence, UserPrevious

def add_usersentence(user, source, chinese, pinyin, english, number):

    l = UserSentence.objects.get_or_create(user=user, source=source, chinese=chinese, pinyin=pinyin, english=english)[0]
    l.number = number
    l.save()
    return l


def wechat_parser(user, source, filepath):
    import os
    import re
    from jieba_fast import cut as j
    from jieba_fast import posseg as pseg
    from googletrans import Translator
    from pinyin_jyutping_sentence import pinyin as pinyin

    # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # print(BASE_DIR)
    # TEXT_DIR = os.path.join(BASE_DIR, 'static/texts/')

    with open(filepath, encoding="utf8", errors='ignore') as f:
        inputSample = f.read()

    rx_sequence=re.compile(r"^(.+?:)\n",re.MULTILINE)
    rx_blanks=re.compile(r"\W+") # to remove blanks and newlines

    messList = []
    spanList = []
    cnt = 1
    for match in rx_sequence.finditer(inputSample):

        name = match.groups()[0]

        span = match.span()
        messDict = {'number':cnt, 'name':name}
        messList.append(messDict)
        spanList.append(span)
        cnt+=1



    messSpan = []
    sentList = []
    for x in range(0,len(spanList)):

        if x<len(spanList)-1:
            # print(x)
            start = spanList[x][1]
            end = spanList[x+1][0]


            mess = inputSample[start:end]
            mess = mess.strip('\n')
            messList[x]['message'] = mess
            name = messList[x]['name']


            pYa = pinyin(mess, spaces=False)
            pYa = pYa.replace(',', ' ')
            pYa = pYa.replace('  ', '')
            pYa = pYa.strip(' .')
            pYa = pYa.replace(' ', ',')
            pY = pYa.replace(',', '\t')  # this is pinyin sentence for display
            pY = name + '\t' + pY
            print(pY)
            # # google trans
            # gTranslated = g(sentence).text # this is google translated sentence for display
            gTranslated = ''

            # jieba
            seg_list = j(mess, cut_all=False)
            segStr = (",".join(seg_list))
            segStr = segStr.replace(',', ' ')
            segStr = segStr.replace('  ', '')
            segStr = segStr.strip(' .')
            segStr = segStr.replace(' ', ',')
            segSent = (segStr.replace(',', '\t'))
            segSent = name + '\t' + segSent
            print(segSent)
            us = add_usersentence(user, source, segSent, pY, gTranslated, x)

            sentence = messList[x]['name']+' '+messList[x]['message']
            sentList.append(sentence)

def wechat_parser_edit(sentence, word, category):
    import os
    import re
    import jieba
    from jieba_fast import posseg as pseg
    from googletrans import Translator
    from pinyin_jyutping_sentence import pinyin as pinyin

    # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # print(BASE_DIR)
    # TEXT_DIR = os.path.join(BASE_DIR, 'static/texts/')
    jieba.add_word(word, freq=None, tag=category)

    inputSample = sentence
    # print('inputSample: '+inputSample)

    rx_sequence=re.compile(r"^(.+?:)",re.MULTILINE)
    # rx_blanks=re.compile(r"\W+") # to remove blanks and newlines

    messList = []
    spanList = []
    cnt = 1
    for match in rx_sequence.finditer(inputSample):

        name = match.groups()[0]
        # print('name: '+name)
        span = match.span()
        # print('span: '+ str(span))
        messDict = {'number':cnt, 'name':name}
        # print('messDict: '+ str(messDict))
        messList.append(messDict)
        spanList.append(span)
        cnt+=1



    messSpan = []
    sentList = []

    start = spanList[0][1]
    end = len(inputSample)

    mess = inputSample[start:end]
    mess = mess.strip('\n')
    messList[0]['message'] = mess
    name = messList[0]['name']

    pYa = pinyin(mess, spaces=False)
    pYa = pYa.replace(',', ' ')
    pYa = pYa.replace('  ', '')
    pYa = pYa.strip(' .')
    pYa = pYa.replace(' ', ',')
    pY = pYa.replace(',', '\t')
    # print(pY)
    # # google trans
    # gTranslated = g(sentence).text # this is google translated sentence for display
    gTranslated = ''

    # jieba
    seg_list = jieba.cut(mess, cut_all=False)
    segStr = (",".join(seg_list))
    segStr = segStr.replace(',', ' ')
    segStr = segStr.replace('  ', '')
    segStr = segStr.strip(' .')
    segStr = segStr.replace(' ', ',')
    segSent = (segStr.replace(',', '\t'))
    segSent = name + '\t' + segSent
    # print(segSent)



    return {'chsentence': segSent, 'pysentence':pY}




if __name__ == '__main__':
    print('Starting Rango population script...')
    wechat_parser()

    # text = (',').join(sentList)
    # text = text.replace(',', '. ')
    # # print(text)
    # inputSample_list = j(text, cut_all=False)
    # inputSampleCnt = dict(Counter(inputSample_list))