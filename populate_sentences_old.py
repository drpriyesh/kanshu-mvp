# reads text in study language and fills the sentences model database
# need to add ability to pickle and pickup where left off

import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
django.setup()

from rango.models import Category, Vocab, Sentence, MotherText, Source
from googletrans import Translator
from pinyin_jyutping_sentence import pinyin as p
from jieba_fast import cut as j
from jieba_fast import posseg as pseg
import re
import sqlite3
from split_into_sentences import split_into_sentences
from populate.CEdict_parser import main


def add_source(name):
    s = Source.objects.get_or_create(title=name)[0]
    s.save()
    return s


def add_vocab(category, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0):

    p = Vocab.objects.get_or_create(category=category, chinese=chinese, english=english, pinyin=pinyin)[0]
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.save()
    return p


def add_cat(name):

    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c


def add_sentence(source, chinese, pinyin, english, number):

    s = Sentence.objects.get_or_create(source=source, chinese=chinese, pinyin=pinyin, english=english)[0]
    s.number = number
    s.save()
    return s

def add_mother(mother, number):

    m = MotherText.objects.get_or_create(mother=mother)[0]
    m.number = number
    m.save()
    return m

CEdict = main()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

flag_ext = {"n": "Common nouns",
             "f": "orientation noun",
             "s": "premises noun",
             "t": "time",
             "nr": "name",
             "ns": "place names",
             "nt": "organization name",
             "nw": "title",
             "nz": "Other proper names",
             "v": "Common verbs",
             "vd": "verb",
             "vn": "noun verb",
             "a": "adjective",
             "ad": "adverb",
             "an": "nouns",
             "d": "adverb",
             "m": "quantifier",
             "q": "quantifier",
             "r": "pronoun",
             "p": "preposition",
             "c": "conjunction",
             "u": "particle",
             "xc": "other word",
             "w": "punctuation",
             "PER": "name",
             "LOC": "Place Name",
             "ORG": "Organization Name",
             "TIME": "Time"}

translator = Translator()
g = translator.translate

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(BASE_DIR)
TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')
print(TEXT_DIR)
# filename = "Harry_Potter_Mandarin"
filename = input("Enter filename (OR press enter for The_Little_Prince_Mandarin): ") or "The_Little_Prince_Mandarin"
print(filename)
s = add_source(filename)
print(TEXT_DIR + filename + '.txt')
# inputPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/static/texts/"
# transPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/static/texts/"

with open(TEXT_DIR + filename + '.txt', encoding="utf8", errors='ignore') as f:
    inputSample = f.read()

with open(TEXT_DIR + 'Harry_Potter_English.txt', encoding="utf8", errors='ignore') as ff:
    transSample = ff.read()

print("Splitting to sentences...")
inputSample_list = j(inputSample, cut_all=False)
iTok = split_into_sentences(inputSample)
tTok = split_into_sentences(transSample)

threshold = 3
learnMode = 1
count = 0
sentence_count = 1
for sentence in iTok:

    # categorize
    posDict = {}
    words = pseg.cut(sentence)
    for w in words:
        posDict[w.word] = w.flag

    # pinyin
    pYa = p(sentence, spaces=False)
    pYa = pYa.replace(',', ' ')
    pYa = pYa.replace('  ', '')
    pYa = pYa.strip(' .')
    pYa = pYa.replace(' ', ',')
    pY = pYa.replace(',', '\t') # this is pinyin sentence for display

    try:
        translator = Translator()
        g = translator.translate
        gTranslated = g(sentence).text
        print('gTranslated: ', gTranslated)
    except AttributeError:
        print('AttributeError')
        gTranslated = ''
        pass
    except ValueError:
        print('ValueError')
        gTranslated = ''
        pass
    except ConnectionError as e:  # This is the correct syntax
        print('ConnectionError')
        gTranslated = ''
        pass
    print('gTranslated: ', gTranslated)
    # jieba
    seg_list = j(sentence, cut_all=False)
    segStr = (",".join(seg_list))
    segStr = segStr.replace(',', ' ')
    segStr = segStr.replace('  ', '')
    segStr = segStr.strip(' .')
    segStr = segStr.replace(' ', ',')
    segSent = (segStr.replace(',', '\t')) # this is chinese sentence for display

    # print(bcolors.HEADER + segSent + bcolors.ENDC)

    # making list of words
    # pinyin list
    pyList = pYa.split(",")
    # chinese list
    segStrList = segStr.split(",")


    add_sentence(s, segSent, pY, gTranslated, sentence_count)
    sentence_count = sentence_count + 1
    print(str(sentence_count) + ": " + sentence)
# sentence_count = 1
# for sentence in tTok:
#     add_mother(sentence, sentence_count)
#     sentence_count = sentence_count + 1
#     print(str(sentence_count) + ":" + sentence)
