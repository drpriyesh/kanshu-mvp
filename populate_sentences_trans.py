# used to populate vocab list. With large text takes too long to complete and also google translate API eventually
# blocks ip, so interim step is to pickle dictionary and use pop_pbin_to_web to add what has been processed to vocab
# database. The code automatically remembers where it last got stuck and starts from there.
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()
import string
from rango.models import Category, Vocab, HSK, Source, Sentence
from requests.exceptions import ConnectionError

from googletrans import Translator as T1
from BingTranslator import Translator as T2

import re
import time
from populate.CEdict_parser import main

from pprint import pprint


import collections

from populate.pickle_dat import pickling, unpickling
import requests

import sys
# source = sys.argv[1]



def pop():
    sourcesi = list(Source.objects.filter())
    for sourcei in sourcesi:
        print(sourcei)
    source = Source.objects.get(title=input("Input source: "))

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')
    POP_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/populate/')
    print(TEXT_DIR)


    sentencesTrans = unpickling(POP_DIR + str(source) + 'sentencesTrans')['data']

    sentences = list(Sentence.objects.filter(source=source).values('number', 'chinese', 'pinyin', 'english'))
    if not sentencesTrans:
        sentencesTrans = []
        pickling(sentencesTrans, POP_DIR + str(source) + 'sentencesTrans')

    for sentence in sentences:
        print(sentence)
        gTranslated = ''
        if sentence['english'] is '' or not sentence['english']:

        # pickling(picklecount, POP_DIR + filename + 'sentencecount')

            try:
                translator = T1()
                g = translator.translate
                gg = g(sentence['chinese'])
                gg.translate({ord(c): None for c in string.whitespace})
                print(g(sentence['chinese']))
                print(gg)
                gTranslated = gg

            except TypeError:
                print('TypeError')

                client_id = "microsoft-translator-text.p.rapidapi.com"
                client_secret = "a1003d1351msheee778817915df7p1e0f28jsn52c63dabf62d"
                translator = T2(client_id, client_secret)
                g = translator.translate
                gg = g(sentence['chinese'])
                gg.translate({ord(c): None for c in string.whitespace})
                print(g(sentence['chinese']))
                print(gg)
                gTranslated = gg
                gTranslated = ''
            except AttributeError:
                print('AttributeError')

                client_id = "microsoft-translator-text.p.rapidapi.com"
                client_secret = "a1003d1351msheee778817915df7p1e0f28jsn52c63dabf62d"
                translator = T2(client_id, client_secret)
                g = translator.translate
                gg = g(sentence['chinese'], 'english')
                gg.translate({ord(c): None for c in string.whitespace})
                print(g(sentence['chinese']))
                print(gg)
                gTranslated = gg
                gTranslated = ''
                pass
            except ValueError:
                print('ValueError')
                gTranslated = ''
                pass
            except ConnectionError as e:  # This is the correct syntax
                print('ConnectionError')
                gTranslated = ''
                pass
            print('gTranslated: ', gTranslated)

        sentence['english'] = gTranslated
        sentencesTrans.append(sentence)
        print(sentence)
        s = Sentence.objects.get(source=source, chinese=sentence['chinese'], number=sentence['number'])
        s.english = gTranslated
        s.save()
        pickling(sentencesTrans, POP_DIR + str(source) + 'sentencesTrans')


# Start execution here!
if __name__=='__main__':

    print('Starting Kanshu population script...')
    pop()