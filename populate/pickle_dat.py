import pickle

from pathlib import Path


def pickling(POS, path):

    # print('Pickling...')
    with open(path, "wb") as file:

        pickle.dump(POS, file)


def unpickling(path):

    unpickledV = {}
    if Path(path).is_file():
        print("Flag pickle file exists: unpickling...")
        with open(path, "rb") as file:
            unpickledV['data'] = pickle.load(file)
            file.close()
    else:
        print ("Pickle file doesn't exist")
        unpickledV['data'] = []
    # print(unpickledV)
    return unpickledV

