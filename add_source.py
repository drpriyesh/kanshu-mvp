# original script that works in terminal (not designed for web app).
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from rango.models import Category, Vocab, Sentence, Source

def add_source(name):
    s = Source.objects.get_or_create(title=name)[0]
    s.save()
    return s

filename = "Harry_Potter_Mandarin"
s = add_source(filename)
print(s)
v = Vocab.objects.all()
print(v[:10])
v.update(source=s)
v.save()

